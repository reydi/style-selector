angular.module 'invoice2goApp'

.factory 'EffecktPositionalModal', ->
  EffecktPositionalModals =
    modalButtonClass: ".effeckt-positional-modal-button"
    modalCloseButtonClass: ".effeckt-modal-close"
    modalWrapClass: "effeckt-positional-modal-wrap"
    modalsList: []
    init: ->
      @bindUIActions()
      return

    bindUIActions: ->
      self = this
      $(@modalButtonClass).on Effeckt.buttonPressedEvent, (e) ->
        e.preventDefault()
        self.openModal $(this)
        return

      $(document).on Effeckt.buttonPressedEvent, @modalCloseButtonClass, (e) ->
        e.preventDefault()
        self.close $(this)
        return

      return

    openModal: ($el) ->
      self = this
      style = $el.data("effeckt-type")
      position = $el.data("effeckt-position")
      buttonPosition = $el.offset()
      buttonSize =
        width: $el.outerWidth()
        height: $el.outerHeight()


      # This only work with page transition
      scrollTop = $(".effeckt-page-active").scrollTop()

      # don't open if already open
      return false  if @contains($el)
      modal = @createModal($("#effeckt-modal-wrap").html(), style, position)
      modal.show()
      console.log modal.width()

      # append to dom, add to list
      @add $el, modal

      # TODO: check if modal window is outside the view.
      # If it's outside switch position

      # change based on position
      switch position
        when "below"
          modal.css
            top: buttonPosition.top + buttonSize.height + scrollTop
            left: buttonPosition.left - (modal.outerWidth() / 2) + buttonSize.width / 2

        when "right"
          modal.css
            top: buttonPosition.top - (modal.outerHeight() / 2) + scrollTop + (buttonSize.height / 2)
            left: buttonPosition.left + buttonSize.width

        when "left"
          modal.css
            top: buttonPosition.top - (modal.outerHeight() / 2) + scrollTop + (buttonSize.height / 2)
            left: buttonPosition.left - (modal.outerWidth())

        else
          "above"
          modal.css
            top: buttonPosition.top - modal.outerHeight() + scrollTop
            left: buttonPosition.left - (modal.outerWidth() / 2) + (buttonSize.width / 2)


      # todo: ensure is on top here.
      # apply effect
      modal.addClass "effeckt-show"
      return

    close: ($el) ->
      self = this
      modal = $el.parents("[class~=\"" + @modalWrapClass + "\"]")
      sender = @getSenderButton(modal)
      modal.removeClass "effeckt-show"
      modal.on Effeckt.transitionAnimationEndEvent, ->
        modal.off Effeckt.transitionAnimationEndEvent
        modal.hide().remove()
        return

      @remove modal
      modal.find(".effeckt-positional-modal").removeClass "effeckt-show"
      modal.addClass "effeckt-hide"  if sender and sender.data("effeckt-needs-hide-class")
      return

    createModal: (content, style, position) ->
      modalWrap = $("<div>").addClass("effeckt-positional-modal-wrap " + style).attr("data-effeckt-position", position)
      modalWrap.html content
      modalWrap

    getSenderButton: (modal) ->
      i = undefined
      i = 0
      len = @modalsList.length

      while i < len
        return @modalsList[i].element  if @modalsList[i].modal.get(0) is modal.get(0)
        i++
      false

    add: ($el, modal) ->

      # add element to dom
      # on the active page wrap
      $(".effeckt-page-active").append modal

      # add element to modal list
      @modalsList.push
        element: $el
        modal: modal

      return

    remove: (modal) ->
      i = undefined
      i = 0
      len = @modalsList.length

      while i < len
        if @modalsList[i].modal.get(0) is modal.get(0)
          @modalsList.splice i, 1
          return true
        i++
      return


    # check if already has modal open
    contains: ($el) ->
      i = undefined
      i = 0
      len = @modalsList.length

      while i < len
        return true  if @modalsList[i].element.get(0) is $el.get(0)
        i++
      false

  EffecktPositionalModals.init()