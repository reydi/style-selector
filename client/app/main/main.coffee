'use strict'

angular.module 'invoice2goApp'
.config ($stateProvider) ->
  $stateProvider
  .state 'main',
    url: '/'
    templateUrl: 'app/main/main.html'
    controller: 'MainCtrl'

  .state 'main.styleSelector',
    url: '^/style-selector'
    views:
      "content@main":
        templateUrl: 'app/style-selector/style-selector.html'

  .state 'main.customTemplate',
    url: '^/custom-template'
    views:
      "content@main":
        templateUrl: 'app/custom-template/custom-template.html'

  .state 'main.documentView',
    url: '^/document-view'
    views:
      "content@main":
        templateUrl: 'app/document-view/document-view.html'