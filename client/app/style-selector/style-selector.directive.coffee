angular.module 'invoice2goApp'
.directive 'styleSelector', ($timeout, $animate) ->
  restrict: 'A'
  scope: false
  link: (scope, element, attrs) ->
    if scope.$last is true
      frame = $(element[0]).parent().parent()
      options =
        horizontal: 1
        itemNav: 'forceCentered'
        smart: 1
        activateMiddle: 1
        activateOn: 'click'
        mouseDragging: 1
        touchDragging: 1
        releaseSwing: 1
        startAt: 4
        scrollBy: 1
        speed: 300
        elasticBounds: 1
        easing: 'swing'
        dragHandle: 1
        dynamicHandle: 1
        clickBar: 1

      active = scope.selector.active
      styles = scope.selector.styles
      colors = scope.selector.colors
      backgrounds = scope.selector.backgrounds

      options.startAt = switch active.selection
        when 0 then active.style
        when 1 then active.color
        when 2 then active.background

      sly = new Sly(frame, options).init()

      sly.toCenter options.startAt, false

      sly.on 'active', (eventName, itemIndex) ->

        doc = $('.document')
        images = doc.children()
        if active.selection == 0 || active.selection == 1
          if active.selection == 0
            unless active.style == itemIndex

              hidingDirection = if itemIndex < active.style then 'right' else 'left'
              showingDirection = if itemIndex < active.style then 'left' else 'right'

              doc.hide 'slide', { direction: hidingDirection }, 150, (->
                active.style = itemIndex
                images[1].onload = ->
                  doc.effect 'slide', { direction: showingDirection }, 150, (->
                    doc.removeAttr 'style'
                  )
                images[1].src = "assets/images/" + styles[active.style] + "/" + styles[active.style] + " " + colors[active.color] + ".png"
              )

          else if active.selection == 1
            unless active.color == itemIndex
              doc.addClass 'flip-out-right'

              images[1].onload = ->
                doc.removeClass 'flip-out-right'
                doc.addClass 'flip-in-left'
                $timeout (->
                  doc.removeClass 'flip-in-left'
                  ), 300

              $timeout (->
                active.color = itemIndex
                images[1].src = "assets/images/" + styles[active.style] + "/" + styles[active.style] + " " + colors[active.color] + ".png"
                ), 150

        else if active.selection == 2
          unless active.background == itemIndex
            hidingDirection = if itemIndex < active.background then 'right' else 'left'
            showingDirection = if itemIndex < active.background then 'left' else 'right'

            $('.document-background').hide 'slide', { direction: hidingDirection }, 150, (->
              active.background = itemIndex
              images[0].onload = ->
                $('.document-background').effect 'slide', { direction: showingDirection }, 150, (->
                  $(".document-background").removeAttr 'style'
                )
              images[0].src = if not itemIndex then '' else "assets/images/Background Select/BG Layers/" + backgrounds[active.background] + ".png"
            )