angular.module 'invoice2goApp'
.controller 'styleSelectorCtrl', ($scope) ->

  styles = [
    'Classic'
    'Modern'
    'Impact'
    'Typewriter'
    'Hip'
    'Creative'
  ]

  colors = [
    'Blue Tie'
    'Mint'
    'Sunset'
    'No Colour'
    'Dark'
    'Steel'
    'Lime'
    'Forest'
    'Soft Orange'
    'Bold Orange'
    'Mandarin'
    'Coin'
    'Goldleaf'
    'Hardhat'
    'Skyblue'
    'Sea'
    'Slate Blue'
    'Purple Tie'
    'Red Purple'
    'Rust'
    'Rouge'
    'Rose'
    'Pinkles'
    'Hot Pink'
  ]

  backgrounds = [
    'No Background'
    'Spanners'
    'Tap'
    'Hammer Saw'
    'Paint Roller'
    'Bolt'
    'Drill'
    'Bobcat'
    'Three Workers'
    'Shovels'
    'Engine'
    'Lawn Mower'
    'Wrench'
    'Lightbulb'
    'Van'
    'Plumber Tools'
    'Screwdriver'
    'Truck'
    'House'
    'Tree'
    'Dumbells'
    'Sports Balls'
    'Knife Fork'
    'Chef Hat'
    'Fruit'
    'Tomato'
    'Camera Icon'
    '35mm Film'
    'TLR Camera'
    'Music Notes'
    'Sheet Music'
    'Vinyl'
    'Mixing Channels'
    'Academic Hat'
    'Scissors'
    'Jewel'
    'Geometric'
    'One Ring'
    'Three Arcs'
    'Two Arcs'
    'Bubbles Outline'
    'Bubbles'
    'Digital Zulu'
    'Circuit Board'
    'Matrix'
    'Blue Squares'
    '3D Blocks'
    'Blue Grid'
    'Cityscape'
    'Diagonal Lines Thick'
    'Diagonal Lines Thin'
    'Diamonds'
    'Steel Floor'
    'The World'
    'Three Diamonds'
    'Triangle Clouds'
  ]

  $scope.selector =
    active:
      selection: 0
      style: 2
      color: 3
      background: 0
    styles: styles
    colors: colors
    backgrounds: backgrounds

  $scope.setSelector = (selection) ->
    $scope.selector.active.selection = selection

  # $scope.next = ->
  #   $scope.selector.active.selection = Math.min($scope.selector.active.selection + 1, 2)

  # $scope.previous = ->
  #   $scope.selector.active.selection = Math.max($scope.selector.active.selection - 1, 0)
