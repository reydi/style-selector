
.factory 'SidebarServices', ->
  new class SidebarServices
    selectElement: (event) ->
      console.log 'wew'
      selectedElement = event.target
      currentX = event.clientX
      currentY = event.clientY
      currentMatrix = selectedElement.getAttributeNS(null, "transform").slice(7,-1).split(' ')

      for matrix, index in currentMatrix
        currentMatrix[index] = parseFloat(currentMatrix[index])

      selectedElement.setAttributeNS null, "onmousemove", "moveElement(event)"
      return

    moveElement: (event) ->
      currentMatrix[4] += event.clientX - currentX
      currentMatrix[5] += event.clientY - currentY
      newMatrix = "matrix(" + currentMatrix.join(' ') + ")"

      selectedElement.setAttributeNS null, "transform", newMatrix

      currentX = event.clientX
      currentY = event.clientY
      return
  