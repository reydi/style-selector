## 1. Frontend Development Environment (AngularJS)

### 1.1 Installation

Make sure you have installed [Grunt] and [Bower]
```sh
$ npm install -g grunt-cli
$ npm install -g bower
```
Install Dependencies:
```sh
$ bower install
$ npm install
```

### 1.2 Run The Angular App

This AngularJS Application consists the prototype of style selector.
After installing the dependencies, you can access it from your `localhost:3000`:

```sh
$ mongod
$ grunt serve
```

[Bower]:http://bower.io/
[Grunt]:http://gruntjs.com/getting-started/
